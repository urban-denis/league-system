package pohlady;

import java.awt.event.ActionListener;
import java.awt.event.ItemListener;
import javax.swing.JPanel;

/**
 * Hlavne okno programu
 * Do okna sa vkladaju jednotlive panely
 * @author Denis
 */
public class HlavnyPohlad extends javax.swing.JFrame {

    /**
     * Vytvori nove okno
     */
    public HlavnyPohlad() {
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(HlavnyPohlad.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(HlavnyPohlad.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(HlavnyPohlad.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(HlavnyPohlad.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        
        initComponents();
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        aHlavnyPanel = new javax.swing.JTabbedPane();
        aZoznamLig = new javax.swing.JComboBox();
        jLabel1 = new javax.swing.JLabel();
        aTlacidloUlozAktivitu = new javax.swing.JButton();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        setTitle("Ligový systém pre stolný tenis");

        jLabel1.setText("Liga");

        aTlacidloUlozAktivitu.setText("Ulož aktivitu");

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                        .addGap(87, 87, 87)
                        .addComponent(aTlacidloUlozAktivitu)
                        .addGap(87, 87, 87))
                    .addGroup(layout.createSequentialGroup()
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(layout.createSequentialGroup()
                                .addGap(34, 34, 34)
                                .addComponent(aZoznamLig, javax.swing.GroupLayout.PREFERRED_SIZE, 197, javax.swing.GroupLayout.PREFERRED_SIZE))
                            .addGroup(layout.createSequentialGroup()
                                .addGap(120, 120, 120)
                                .addComponent(jLabel1)))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)))
                .addComponent(aHlavnyPanel, javax.swing.GroupLayout.DEFAULT_SIZE, 625, Short.MAX_VALUE))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(aHlavnyPanel)
            .addGroup(layout.createSequentialGroup()
                .addGap(35, 35, 35)
                .addComponent(jLabel1)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(aZoznamLig, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(18, 18, 18)
                .addComponent(aTlacidloUlozAktivitu)
                .addGap(391, 391, 391))
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JTabbedPane aHlavnyPanel;
    private javax.swing.JButton aTlacidloUlozAktivitu;
    private javax.swing.JComboBox aZoznamLig;
    private javax.swing.JLabel jLabel1;
    // End of variables declaration//GEN-END:variables

    /**
     *
     * @param paListener
     */
    public void addUlozitAktivituListener(ActionListener paListener) {
        aTlacidloUlozAktivitu.addActionListener(paListener);
    }
    
    /**
     * Prida novy panel
     * @param paNazov nazov
     * @param paPanel panel
     */
    public void pridajTab(String paNazov, JPanel paPanel) {
        aHlavnyPanel.addTab(paNazov, paPanel);
    }
    
    /**
     * Vymaze zoznam lig
     */
    public void vymazZoznamLig() {
        aZoznamLig.removeAllItems();
    }
    
    /**
     * Vlozi ligu do zoznamu lig
     * @param paNazov nazov ligy
     * @param paHodnota liga
     */
    public void vlozDoZoznamuLig(String paNazov, Object paHodnota) {
        aZoznamLig.addItem(new PrvokZoznamu(paNazov, paHodnota));
    }
    
    /**
     * Vracia true ak je v zozname aspon 1 liga
     * @return ture/false
     */
    public boolean suVZoznameLig() {
        return aZoznamLig.getItemCount() > 0;
    }
    
    /**
     * Detekuje zmenu ligy pouzivatelom
     * @param paVlozitTimListener
     */
    public void zmenaLigyListener(ItemListener paVlozitTimListener) {
        aZoznamLig.addItemListener(paVlozitTimListener);
    }
    
    /**
     * Zobrazi hl. okno
     */
    public void zobraz() {
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                setVisible(true);
            }
        });
    }

    /**
     * Vrati vybratu ligu v zozname
     * @return liga
     */
    public Object dajVybranuLigu() {
        return aZoznamLig.getSelectedItem();
    }

}
