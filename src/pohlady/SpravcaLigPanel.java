package pohlady;

import java.awt.event.ActionListener;
import javax.swing.table.DefaultTableModel;

/**
 * Panel pre spravovanie lig
 * @author Denis
 */
public class SpravcaLigPanel extends javax.swing.JPanel {
    private final DefaultTableModel aTabulkaTimovModel;
    private final DefaultTableModel aTabulkaLigModel;
    /**
     * Vytvori novy panel
     * @param paHlavnyPohlad referencia na hl. pohlad
     */
    public SpravcaLigPanel(HlavnyPohlad paHlavnyPohlad) {
        initComponents();
        aTabulkaTimovModel = (DefaultTableModel) aTabulkaTimov.getModel();
        aTabulkaLigModel = (DefaultTableModel) aTabulkaLig.getModel();
        paHlavnyPohlad.pridajTab("Správca líg", this);
        paHlavnyPohlad.pack();
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        scrollPane1 = new javax.swing.JScrollPane();
        aTabulkaTimov = new javax.swing.JTable();
        aTlacidloLigaVymazat = new javax.swing.JButton();
        aTlacidloLigaUpravit = new javax.swing.JButton();
        aTlacidloLigaVlozit = new javax.swing.JButton();
        aTextFieldLiga = new javax.swing.JTextField();
        labelNazovLigy = new javax.swing.JLabel();
        labelEditorLig = new javax.swing.JLabel();
        scrollPane2 = new javax.swing.JScrollPane();
        aTabulkaLig = new javax.swing.JTable();
        aTlacidloTimVymazat = new javax.swing.JButton();
        aTlacidloTimUpravit = new javax.swing.JButton();
        aTlacidloTimVlozit = new javax.swing.JButton();
        labelNazovTimu = new javax.swing.JLabel();
        aTextFieldTim = new javax.swing.JTextField();
        labelEditorTimov = new javax.swing.JLabel();

        setBackground(new java.awt.Color(255, 255, 255));
        setPreferredSize(new java.awt.Dimension(625, 564));

        aTabulkaTimov.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {
                "P. č", "Mužstvá"
            }
        ) {
            Class[] types = new Class [] {
                java.lang.Integer.class, java.lang.String.class
            };
            boolean[] canEdit = new boolean [] {
                false, false
            };

            public Class getColumnClass(int columnIndex) {
                return types [columnIndex];
            }

            public boolean isCellEditable(int rowIndex, int columnIndex) {
                return canEdit [columnIndex];
            }
        });
        aTabulkaTimov.getTableHeader().setReorderingAllowed(false);
        aTabulkaTimov.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                aTabulkaTimovMouseClicked(evt);
            }
        });
        scrollPane1.setViewportView(aTabulkaTimov);
        if (aTabulkaTimov.getColumnModel().getColumnCount() > 0) {
            aTabulkaTimov.getColumnModel().getColumn(0).setResizable(false);
            aTabulkaTimov.getColumnModel().getColumn(0).setPreferredWidth(50);
            aTabulkaTimov.getColumnModel().getColumn(1).setResizable(false);
            aTabulkaTimov.getColumnModel().getColumn(1).setPreferredWidth(400);
        }

        aTlacidloLigaVymazat.setText("Vymazať");

        aTlacidloLigaUpravit.setText("Upraviť");

        aTlacidloLigaVlozit.setText("Vložiť");

        labelNazovLigy.setText("Názov");

        labelEditorLig.setText("Editor Líg");

        aTabulkaLig.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {
                "P. č", "Ligy"
            }
        ) {
            Class[] types = new Class [] {
                java.lang.Integer.class, java.lang.String.class
            };
            boolean[] canEdit = new boolean [] {
                false, false
            };

            public Class getColumnClass(int columnIndex) {
                return types [columnIndex];
            }

            public boolean isCellEditable(int rowIndex, int columnIndex) {
                return canEdit [columnIndex];
            }
        });
        aTabulkaLig.getTableHeader().setReorderingAllowed(false);
        aTabulkaLig.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                aTabulkaLigMouseClicked(evt);
            }
        });
        scrollPane2.setViewportView(aTabulkaLig);
        if (aTabulkaLig.getColumnModel().getColumnCount() > 0) {
            aTabulkaLig.getColumnModel().getColumn(0).setResizable(false);
            aTabulkaLig.getColumnModel().getColumn(0).setPreferredWidth(50);
            aTabulkaLig.getColumnModel().getColumn(1).setResizable(false);
            aTabulkaLig.getColumnModel().getColumn(1).setPreferredWidth(400);
        }

        aTlacidloTimVymazat.setText("Vymazať");

        aTlacidloTimUpravit.setText("Upraviť");

        aTlacidloTimVlozit.setText("Vložit");

        labelNazovTimu.setText("Názov");

        labelEditorTimov.setText("Editor mužstiev");

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(this);
        this.setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addContainerGap(48, Short.MAX_VALUE)
                        .addComponent(aTlacidloTimVlozit)
                        .addGap(18, 18, 18)
                        .addComponent(aTlacidloTimUpravit, javax.swing.GroupLayout.PREFERRED_SIZE, 75, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(18, 18, 18)
                        .addComponent(aTlacidloTimVymazat))
                    .addGroup(layout.createSequentialGroup()
                        .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(labelEditorTimov)
                            .addGroup(layout.createSequentialGroup()
                                .addComponent(labelNazovTimu)
                                .addGap(18, 18, 18)
                                .addComponent(aTextFieldTim, javax.swing.GroupLayout.PREFERRED_SIZE, 195, javax.swing.GroupLayout.PREFERRED_SIZE)))))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 54, Short.MAX_VALUE)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(aTlacidloLigaVlozit)
                        .addGap(18, 18, 18)
                        .addComponent(aTlacidloLigaUpravit, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addGap(18, 18, 18)
                        .addComponent(aTlacidloLigaVymazat))
                    .addComponent(labelEditorLig)
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(labelNazovLigy)
                        .addGap(18, 18, 18)
                        .addComponent(aTextFieldLiga, javax.swing.GroupLayout.PREFERRED_SIZE, 197, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addContainerGap(35, Short.MAX_VALUE))
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                .addComponent(scrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 307, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(scrollPane2, javax.swing.GroupLayout.PREFERRED_SIZE, 0, Short.MAX_VALUE))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGap(26, 26, 26)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(labelEditorTimov)
                    .addComponent(labelEditorLig))
                .addGap(18, 18, 18)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(labelNazovTimu)
                    .addComponent(aTextFieldTim, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(labelNazovLigy)
                    .addComponent(aTextFieldLiga, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(18, 18, 18)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(aTlacidloTimVlozit)
                    .addComponent(aTlacidloTimUpravit)
                    .addComponent(aTlacidloTimVymazat)
                    .addComponent(aTlacidloLigaVlozit)
                    .addComponent(aTlacidloLigaUpravit)
                    .addComponent(aTlacidloLigaVymazat))
                .addGap(18, 18, Short.MAX_VALUE)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(scrollPane1)
                    .addComponent(scrollPane2)))
        );
    }// </editor-fold>//GEN-END:initComponents

    private void aTabulkaTimovMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_aTabulkaTimovMouseClicked
        aTextFieldTim.setText(String.valueOf(aTabulkaTimov.getValueAt(aTabulkaTimov.getSelectedRow(), 1)));
    }//GEN-LAST:event_aTabulkaTimovMouseClicked

    private void aTabulkaLigMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_aTabulkaLigMouseClicked
        aTextFieldLiga.setText(String.valueOf(aTabulkaLig.getValueAt(aTabulkaLig.getSelectedRow(), 1)));
    }//GEN-LAST:event_aTabulkaLigMouseClicked

    private void tabulkaTimovMouseClicked(java.awt.event.MouseEvent evt) {
        aTextFieldTim.setText(String.valueOf(aTabulkaTimov.getValueAt(aTabulkaTimov.getSelectedRow(), 1)));
    }
    
    private void tabulkaLigMouseClicked(java.awt.event.MouseEvent evt) {
        aTextFieldLiga.setText(String.valueOf(aTabulkaLig.getValueAt(aTabulkaLig.getSelectedRow(), 1)));
    }
    
    /**
     *
     * @param paVlozitTimListener
     */
    public void addVlozitTimListener(ActionListener paVlozitTimListener) {
        aTlacidloTimVlozit.addActionListener(paVlozitTimListener);
    }
    
    /**
     *
     * @param paUpravitTimListener
     */
    public void addUpravitTimListener(ActionListener paUpravitTimListener) {
        aTlacidloTimUpravit.addActionListener(paUpravitTimListener);
    }
    
    /**
     *
     * @param paVymazatTimListener
     */
    public void addVymazatTimListener(ActionListener paVymazatTimListener) {
        aTlacidloTimVymazat.addActionListener(paVymazatTimListener);
    }
    
    /**
     *
     * @param paVlozitLiguListener
     */
    public void addVlozitLiguListener(ActionListener paVlozitLiguListener) {
        aTlacidloLigaVlozit.addActionListener(paVlozitLiguListener);
    }
    
    /**
     *
     * @param paUpravitLiguListener
     */
    public void addUpravitLiguListener(ActionListener paUpravitLiguListener) {
        aTlacidloLigaUpravit.addActionListener(paUpravitLiguListener);
    }
    
    /**
     *
     * @param paVymazatLiguListener
     */
    public void addVymazatLiguListener(ActionListener paVymazatLiguListener) {
        aTlacidloLigaVymazat.addActionListener(paVymazatLiguListener);
    }
    
    /**
     * Vlozenie timu do tabulky timov
     * @param paCislo cislo timu
     * @param paTim tim
     */
    public void vlozDoTabulkyTimov(int paCislo, Object paTim) {
        aTabulkaTimovModel.insertRow(aTabulkaTimov.getRowCount(), new Object[] {paCislo + ". ", paTim});
    }
    
    /**
     * Vlozenie ligy do tabulky lig
     * @param paCislo cislo ligy
     * @param paLiga liga
     */
    public void vlozDoTabulkyLig(int paCislo, Object paLiga) {
        aTabulkaLigModel.insertRow(aTabulkaLig.getRowCount(), new Object[] {paCislo + ". ", paLiga});
    }
    
    /**
     * Vymazanie tabulky timov
     */
    public void vymazTabulkuTimov() {
        for(int i = aTabulkaTimovModel.getRowCount() - 1; i >= 0 ; i--) {
            aTabulkaTimovModel.removeRow(i);
        }
    }
    
    /**
     * Vymazanie tabulky lig
     */
    public void vymazTabulkuLig() {
        for(int i = aTabulkaLigModel.getRowCount() - 1; i >= 0 ; i--) {
            aTabulkaLigModel.removeRow(i);
        }
    }
    
    /**
     * Vracia nazov timu z textoveho policka
     * @return nazov timu
     */
    public String dajNazovTimu() {
        return aTextFieldTim.getText();
    }
    
    /**
     * Vracia nazov ligy z textoveho policka
     * @return nazov ligy
     */
    public String dajNazovLigy() {
        return aTextFieldLiga.getText();
    }

    /**
     * Vracia vybraty tim v tabulke timov
     * @return tim
     */
    public Object dajTim() {
        if(aTabulkaTimov.getSelectedRow() >= 0) {
            return aTabulkaTimov.getValueAt(aTabulkaTimov.getSelectedRow(), 1);
        }
        return null;
    }
    
    /**
     * Vracia vybratu ligu v tabulke lig
     * @return liga
     */
    public Object dajLigu() {
        if(aTabulkaLig.getSelectedRow() >= 0) {
            return aTabulkaLig.getValueAt(aTabulkaLig.getSelectedRow(), 1);
        }
        return null;
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JTable aTabulkaLig;
    private javax.swing.JTable aTabulkaTimov;
    private javax.swing.JTextField aTextFieldLiga;
    private javax.swing.JTextField aTextFieldTim;
    private javax.swing.JButton aTlacidloLigaUpravit;
    private javax.swing.JButton aTlacidloLigaVlozit;
    private javax.swing.JButton aTlacidloLigaVymazat;
    private javax.swing.JButton aTlacidloTimUpravit;
    private javax.swing.JButton aTlacidloTimVlozit;
    private javax.swing.JButton aTlacidloTimVymazat;
    private javax.swing.JLabel labelEditorLig;
    private javax.swing.JLabel labelEditorTimov;
    private javax.swing.JLabel labelNazovLigy;
    private javax.swing.JLabel labelNazovTimu;
    private javax.swing.JScrollPane scrollPane1;
    private javax.swing.JScrollPane scrollPane2;
    // End of variables declaration//GEN-END:variables
}
