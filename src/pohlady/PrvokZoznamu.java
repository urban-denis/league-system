
package pohlady;

/**
 * Trieda pre prvok v zoznamoch v hl. okne
 * @author Denis
 */
public class PrvokZoznamu {
    private final String aNazov;
    private final Object aHodnota;

    /**
     * Vytvori prvok zoznamu
     * @param paNazov viditelny nazov prvka
     * @param paHodnota hodnota prvka
     */
    public PrvokZoznamu(String paNazov, Object paHodnota) {
        aNazov = paNazov;
        aHodnota = paHodnota;
    }

    @Override
    public String toString() {
        return aNazov;
    }

    /**
     * Vracia nazov prvka
     * @return nazov prvka
     */
    public String dajNazov() {
        return aNazov;
    }

    /**
     * Vracia hodnotu prvka
     * @return hodnota prvka
     */
    public Object dajHodnotu() {
        return aHodnota;
    }
}
