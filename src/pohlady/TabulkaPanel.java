package pohlady;

import java.awt.event.ActionListener;
import javax.swing.table.DefaultTableModel;

/**
 * Panel pre ligovu tabulku
 * @author Denis
 */
public class TabulkaPanel extends javax.swing.JPanel {
    private final DefaultTableModel aTabulkaModel;
    /**
     * Vytvara novy panel
     * @param paHlavnyPohlad
     */
    public TabulkaPanel(HlavnyPohlad paHlavnyPohlad) {
        initComponents();
        aTabulkaModel = (DefaultTableModel) aTabulka.getModel();
        paHlavnyPohlad.pridajTab("Tabuľka", this);
        paHlavnyPohlad.pack();
    }

       @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jScrollPane1 = new javax.swing.JScrollPane();
        aTabulka = new javax.swing.JTable();
        aTlacidloPredosle = new javax.swing.JButton();
        aTlacidloDalsie = new javax.swing.JButton();
        aLabelKolo = new javax.swing.JLabel();

        setBackground(new java.awt.Color(255, 255, 255));

        aTabulka.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {
                "P.č", "Mužstvo", "Zápasy", "V", "P", "R", "Skóre", "Body"
            }
        ) {
            Class[] types = new Class [] {
                java.lang.Integer.class, java.lang.String.class, java.lang.Integer.class, java.lang.Integer.class, java.lang.Integer.class, java.lang.Integer.class, java.lang.String.class, java.lang.Integer.class
            };
            boolean[] canEdit = new boolean [] {
                false, false, false, false, false, false, false, false
            };

            public Class getColumnClass(int columnIndex) {
                return types [columnIndex];
            }

            public boolean isCellEditable(int rowIndex, int columnIndex) {
                return canEdit [columnIndex];
            }
        });
        aTabulka.getTableHeader().setReorderingAllowed(false);
        jScrollPane1.setViewportView(aTabulka);
        if (aTabulka.getColumnModel().getColumnCount() > 0) {
            aTabulka.getColumnModel().getColumn(0).setResizable(false);
            aTabulka.getColumnModel().getColumn(0).setPreferredWidth(20);
            aTabulka.getColumnModel().getColumn(1).setResizable(false);
            aTabulka.getColumnModel().getColumn(1).setPreferredWidth(260);
            aTabulka.getColumnModel().getColumn(2).setResizable(false);
            aTabulka.getColumnModel().getColumn(2).setPreferredWidth(40);
            aTabulka.getColumnModel().getColumn(3).setResizable(false);
            aTabulka.getColumnModel().getColumn(3).setPreferredWidth(20);
            aTabulka.getColumnModel().getColumn(4).setResizable(false);
            aTabulka.getColumnModel().getColumn(4).setPreferredWidth(20);
            aTabulka.getColumnModel().getColumn(5).setResizable(false);
            aTabulka.getColumnModel().getColumn(5).setPreferredWidth(20);
            aTabulka.getColumnModel().getColumn(6).setResizable(false);
            aTabulka.getColumnModel().getColumn(6).setPreferredWidth(50);
            aTabulka.getColumnModel().getColumn(7).setResizable(false);
            aTabulka.getColumnModel().getColumn(7).setPreferredWidth(40);
        }

        aTlacidloPredosle.setText("Predchádzajúce");

        aTlacidloDalsie.setText("Ďalšie");
        aTlacidloDalsie.setMaximumSize(new java.awt.Dimension(63, 19));

        aLabelKolo.setText("Kolo");

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(this);
        this.setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(aLabelKolo)
                .addGap(42, 42, 42)
                .addComponent(aTlacidloPredosle, javax.swing.GroupLayout.PREFERRED_SIZE, 136, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(18, 18, 18)
                .addComponent(aTlacidloDalsie, javax.swing.GroupLayout.PREFERRED_SIZE, 107, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(38, 38, 38))
            .addComponent(jScrollPane1, javax.swing.GroupLayout.DEFAULT_SIZE, 625, Short.MAX_VALUE)
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(aTlacidloPredosle, javax.swing.GroupLayout.PREFERRED_SIZE, 18, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(aLabelKolo)
                    .addComponent(aTlacidloDalsie, javax.swing.GroupLayout.PREFERRED_SIZE, 18, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 549, javax.swing.GroupLayout.PREFERRED_SIZE))
        );
    }// </editor-fold>//GEN-END:initComponents

    /**
     * 
     * @param paDalsieListener
     */
    public void addDalsieListener(ActionListener paDalsieListener) {
        aTlacidloDalsie.addActionListener(paDalsieListener);
    }

    /**
     *
     * @param paPredosleListener
     */
    public void addPredosleListener(ActionListener paPredosleListener) {
        aTlacidloPredosle.addActionListener(paPredosleListener);
    }
    
    /**
     * Vlozi tim so statistikami do tabulky
     * @param paCislo poradove cislo
     * @param paTim nazov timu
     * @param paPocetZapasov pocet zapasov
     * @param paVyhry pocet vyhier
     * @param paPrehry pocet prehier
     * @param paRemizy pocet remiz
     * @param paSkore skore
     * @param paBody celkove body
     */
    public void vlozDoTabulky(int paCislo, String paTim, String paPocetZapasov, String paVyhry, String paPrehry,
                              String paRemizy, String paSkore, String paBody) {
        aTabulkaModel.insertRow(aTabulka.getRowCount(), new Object[] {paCislo, paTim, paPocetZapasov,
                              paVyhry, paPrehry, paRemizy, paSkore, paBody});
    }
    
    /**
     * Vymaze tabulku
     */
    public void vymazTabulku() {
        for(int i = aTabulkaModel.getRowCount() - 1; i >= 0 ; i--) {
            aTabulkaModel.removeRow(i);
        }
    }
    
    /**
     * Skryje tlacidlo dalsie
     */
    public void skryDalsie() {
        aTlacidloDalsie.setVisible(false);
    }
    
    /**
     * Skryje tlacidlo predchadzajuce
     */
    public void skryPredosle() {
        aTlacidloPredosle.setVisible(false);
    }
    
    /**
     * Odkryje tlacidlo dalsie
     */
    public void ukazDalsie() {
        aTlacidloDalsie.setVisible(true);
    }
    
    /**
     * Odkryje tlacidlo predchadzajuce
     */
    public void ukazPredosle() {
        aTlacidloPredosle.setVisible(true);
    }
    
    /**
     * Nastavi aktualne kolo
     * @param paKolo kolo
     */
    public void nastavKolo(String paKolo) {
        aLabelKolo.setText(paKolo);
    }
    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JLabel aLabelKolo;
    private javax.swing.JTable aTabulka;
    private javax.swing.JButton aTlacidloDalsie;
    private javax.swing.JButton aTlacidloPredosle;
    private javax.swing.JScrollPane jScrollPane1;
    // End of variables declaration//GEN-END:variables
}
