package pohlady;

import java.awt.event.ActionListener;
import java.awt.event.MouseListener;
import javax.swing.table.DefaultTableModel;

/**
 * Panel pre spravovanie zapasov
 * 
 * @author Denis
 */
public class SpravcaZapasovPanel extends javax.swing.JPanel {
    private final DefaultTableModel aTabulkaModel;
    /**
     * Konstruktor pre vytvorenie panela
     * @param paHlavnyPohlad referencia na hl. pohlad
     */
    public SpravcaZapasovPanel(HlavnyPohlad paHlavnyPohlad) {
        initComponents();
        aTabulkaModel = (DefaultTableModel) aTabulka.getModel();
        paHlavnyPohlad.pridajTab("Správca zápasov", this);
        paHlavnyPohlad.pack();
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        aSkoreDomaci = new javax.swing.JTextField();
        aZoznamDomaci = new javax.swing.JComboBox();
        labelDomaci = new javax.swing.JLabel();
        aZoznamHostia = new javax.swing.JComboBox();
        labelHostia = new javax.swing.JLabel();
        aSkoreHostia = new javax.swing.JTextField();
        aTlacidloVlozit = new javax.swing.JButton();
        labelDvojbodka = new javax.swing.JLabel();
        labelKolo = new javax.swing.JLabel();
        jScrollPane4 = new javax.swing.JScrollPane();
        aTabulka = new javax.swing.JTable();
        aSpinnerKolo = new javax.swing.JSpinner();
        aTlacidloVymazat = new javax.swing.JButton();
        aTlacidloUpravit = new javax.swing.JButton();

        setBackground(new java.awt.Color(255, 255, 255));

        labelDomaci.setText("Domáci");

        labelHostia.setText("Hostia");

        aTlacidloVlozit.setText("Vložiť");

        labelDvojbodka.setText(":");

        labelKolo.setText("Kolo:");

        aTabulka.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {
                "Kolo", "Zápas"
            }
        ) {
            Class[] types = new Class [] {
                java.lang.Integer.class, java.lang.Object.class
            };
            boolean[] canEdit = new boolean [] {
                false, false
            };

            public Class getColumnClass(int columnIndex) {
                return types [columnIndex];
            }

            public boolean isCellEditable(int rowIndex, int columnIndex) {
                return canEdit [columnIndex];
            }
        });
        jScrollPane4.setViewportView(aTabulka);
        if (aTabulka.getColumnModel().getColumnCount() > 0) {
            aTabulka.getColumnModel().getColumn(0).setResizable(false);
            aTabulka.getColumnModel().getColumn(0).setPreferredWidth(40);
            aTabulka.getColumnModel().getColumn(1).setResizable(false);
            aTabulka.getColumnModel().getColumn(1).setPreferredWidth(500);
        }

        aTlacidloVymazat.setText("Vymazať");

        aTlacidloUpravit.setText("Upraviť");

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(this);
        this.setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jScrollPane4, javax.swing.GroupLayout.DEFAULT_SIZE, 625, Short.MAX_VALUE)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(labelKolo)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(aSpinnerKolo, javax.swing.GroupLayout.PREFERRED_SIZE, 45, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addComponent(aZoznamDomaci, javax.swing.GroupLayout.PREFERRED_SIZE, 150, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(18, 18, 18)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(aTlacidloVlozit)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(aTlacidloUpravit, javax.swing.GroupLayout.PREFERRED_SIZE, 85, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(aTlacidloVymazat, javax.swing.GroupLayout.PREFERRED_SIZE, 78, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(aSkoreDomaci, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(labelDvojbodka)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(aSkoreHostia, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(18, 18, 18)
                        .addComponent(aZoznamHostia, javax.swing.GroupLayout.PREFERRED_SIZE, 150, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
            .addGroup(layout.createSequentialGroup()
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(labelDomaci)
                .addGap(215, 215, 215)
                .addComponent(labelHostia)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGap(22, 22, 22)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(labelDomaci)
                    .addComponent(labelHostia))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(aZoznamDomaci, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(aZoznamHostia, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(aSkoreDomaci, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(labelDvojbodka)
                    .addComponent(aSkoreHostia, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(18, 18, 18)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(aTlacidloVlozit)
                    .addComponent(labelKolo)
                    .addComponent(aSpinnerKolo, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(aTlacidloUpravit)
                    .addComponent(aTlacidloVymazat))
                .addGap(18, 18, 18)
                .addComponent(jScrollPane4, javax.swing.GroupLayout.DEFAULT_SIZE, 357, Short.MAX_VALUE))
        );
    }// </editor-fold>//GEN-END:initComponents

    /**
     * 
     * @param paMouseClickedListener
     */
    public void addTabulkaMouseClicked(MouseListener paMouseClickedListener) {
        aTabulka.addMouseListener(paMouseClickedListener);
    }

    /**
     *
     * @param paVlozitZapasListener
     */
    public void addVlozitZapasListener(ActionListener paVlozitZapasListener) {
        aTlacidloVlozit.addActionListener(paVlozitZapasListener);
    }

    /**
     *
     * @param paUpravitZapasListener
     */
    public void addUpravitZapasListener(ActionListener paUpravitZapasListener) {
        aTlacidloUpravit.addActionListener(paUpravitZapasListener);
    }

    /**
     *
     * @param paVymazatZapasListener
     */
    public void addVymazatZapasListener(ActionListener paVymazatZapasListener) {
        aTlacidloVymazat.addActionListener(paVymazatZapasListener);
    }

    /**
     * Resetuje vsetky vstupy na prazdne hodnoty
     */
    public void resetujVstupy() {
        aSpinnerKolo.setValue(1);
        aSkoreDomaci.setText("0");
        aSkoreHostia.setText("0");
        this.vymazZoznamy();
        this.vymazTabulku();
    }

    private void vymazZoznamy() {
        aZoznamDomaci.removeAllItems();
        aZoznamHostia.removeAllItems();
    }

    private void vymazTabulku() {
        for (int i = aTabulkaModel.getRowCount() - 1; i >= 0; i--) {
            aTabulkaModel.removeRow(i);
        }
    }

    /**
     * Vlozi do zoznamov timov
     * @param paNazov nazov timu
     * @param paTim tim
     */
    public void vlozDoZoznamov(String paNazov, Object paTim) {
        aZoznamDomaci.addItem(new PrvokZoznamu(paNazov, paTim));
        aZoznamHostia.addItem(new PrvokZoznamu(paNazov, paTim));
    }

    /**
     * Vlozi zapas do tabulky
     * @param paKolo kolo
     * @param paZapas zapas
     */
    public void vlozDoTabulky(Object paKolo, Object paZapas) {
        aTabulkaModel.insertRow(aTabulka.getRowCount(), new Object[]{paKolo, paZapas});
    }

    /**
     * Vracia oznacene kolo v tabulke
     * @return
     */
    public Object dajKolo() {
        if (aTabulka.getSelectedRow() >= 0) {
            return aTabulka.getValueAt(aTabulka.getSelectedRow(), 0);
        }
        return null;
    }

    /**
     * Vracia oznaceny zapas v tabulke
     * @return zapas
     */
    public Object dajZapas() {
        if (aTabulka.getSelectedRow() >= 0) {
            return aTabulka.getValueAt(aTabulka.getSelectedRow(), 1);
        }
        return null;
    }

    /**
     * Metoda vracia oznaceny tim domacich v zozname
     * @return tim
     */
    public Object dajDomacich() {
        if (aZoznamDomaci.getItemCount() > 0) {
            PrvokZoznamu item = (PrvokZoznamu) aZoznamDomaci.getSelectedItem();
            return item.dajHodnotu();
        }
        return null;
    }

    /**
     * Metoda vracia oznaceny tim hosti v zozname
     * @return tim
     */
    public Object dajHosti() {
        if (aZoznamHostia.getItemCount() > 0) {
            PrvokZoznamu item = (PrvokZoznamu) aZoznamHostia.getSelectedItem();
            return item.dajHodnotu();
        }
        return null;
    }

    /**
     * Metoda vracia skore domacich z textoveho pola
     * @return skore domacich
     */
    public String dajSkoreDomacich() {
        return aSkoreDomaci.getText();
    }

    /**
     * Metoda vracia skore hosti z textoveho pola
     * @return skore hosti
     */
    public String dajSkoreHosti() {
        return aSkoreHostia.getText();
    }

    /**
     * Vracia cislo kola zo spinnera
     * @return cislo kola
     */
    public int dajCisloKola() {
        return (Integer) aSpinnerKolo.getValue();
    }

    /**
     * Vlozi tim do zoznamu domacich
     * @param paDomaci tim
     */
    public void vlozDoZoznamuDomacich(Object paDomaci) {
        aZoznamDomaci.addItem(new PrvokZoznamu(paDomaci.toString(), paDomaci));
    }

    /**
     * Vlozi tim do zoznamu hosti
     * @param paHostia tim
     */
    public void vlozDoZoznamuHosti(Object paHostia) {
        aZoznamHostia.addItem(new PrvokZoznamu(paHostia.toString(), paHostia));
    }

    /**
     * Nastavi textove pole skore domacich
     * @param paSkoreDomacich skore domacich
     */
    public void nastavSkoreDomacich(int paSkoreDomacich) {
        aSkoreDomaci.setText("" + paSkoreDomacich);
    }

    /**
     * Nastavi textove pole skore hosti
     * @param paSkoreHosti skroe hosti
     */
    public void nastavSkoreHosti(int paSkoreHosti) {
        aSkoreHostia.setText("" + paSkoreHosti);
    }

    /**
     * Nastavenie timu domacich
     * @param paDomaci tim
     */
    public void nastavTimDomaci(Object paDomaci) {
        for (int i = 0; i < aZoznamDomaci.getItemCount(); i++) {
            PrvokZoznamu item = (PrvokZoznamu) aZoznamDomaci.getItemAt(i);
            if (paDomaci.equals(item.dajHodnotu())) {
                aZoznamDomaci.setSelectedItem(item);
            }
        }
    }

    /**
     * Nastavenie timu hosti
     * @param paHostia tim
     */
    public void nastavTimHostia(Object paHostia) {
        for (int i = 0; i < aZoznamHostia.getItemCount(); i++) {
            PrvokZoznamu item = (PrvokZoznamu) aZoznamHostia.getItemAt(i);
            if (paHostia.equals(item.dajHodnotu())) {
                aZoznamHostia.setSelectedItem(item);
            }
        }
    }

    /**
     * Nastavenie kola v paneli
     * @param paCislo cislo kola
     */
    public void nastavKolo(int paCislo) {
        aSpinnerKolo.setValue(paCislo);
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JTextField aSkoreDomaci;
    private javax.swing.JTextField aSkoreHostia;
    private javax.swing.JSpinner aSpinnerKolo;
    private javax.swing.JTable aTabulka;
    private javax.swing.JButton aTlacidloUpravit;
    private javax.swing.JButton aTlacidloVlozit;
    private javax.swing.JButton aTlacidloVymazat;
    private javax.swing.JComboBox aZoznamDomaci;
    private javax.swing.JComboBox aZoznamHostia;
    private javax.swing.JScrollPane jScrollPane4;
    private javax.swing.JLabel labelDomaci;
    private javax.swing.JLabel labelDvojbodka;
    private javax.swing.JLabel labelHostia;
    private javax.swing.JLabel labelKolo;
    // End of variables declaration//GEN-END:variables
}
