package subory;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.PrintWriter;

/**
 * Trieda pre jednoduchy zapisovac
 * @author Denis
 */
public class Zapisovac {
    private PrintWriter aZapisovac;
    
    /**
     * Konstruktor pre zapisovac
     * Vytvarame zapisovac podla zadaneho suboru
     * Obsahuje vynimku pre chybu pri otvarani
     * @param paSubor - subor
     */
    public Zapisovac(File paSubor) {
        aZapisovac = null;
        try {
            aZapisovac = new PrintWriter(paSubor);
        } catch (FileNotFoundException ex) {
            System.out.println("Problém s otvorením súboru: \n" + ex.getMessage());
        }
    }
    
    /**
     * Metoda pre zapis riadka do suboru
     * @param paRiadok - riadok
     */
    public void zapis(String paRiadok) {
        aZapisovac.println(paRiadok);
    }
    
    /**
     * Metoda pre zatvorenie zapisovaca
     */
    public void zatvor() {
        if(aZapisovac != null) {
            aZapisovac.close();
        }
    }
}
