package kontrolery;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import logika.Liga;
import logika.Tim;
import modely.HlavnyModel;
import pohlady.SpravcaLigPanel;

/**
 * Kontroler pre panel spravcu lig
 * Komunikuje s hl. modelom a vykonava vsetky akcie spojene so spravcom lig
 * 
 * @author Denis
 */
public final class SpravcaLigKontroler implements IKontroler {
    private final HlavnyModel aModel;
    private final SpravcaLigPanel aSpravcaLigPohlad;
    private final HlavnyKontroler aHlavnyKontroler;

    /**
     * Konstruktor pre Kontroler
     * Vytvara jednotlive listenery pre akcie tlacidiel
     * @param paModel referencia na hlavny model
     * @param paPohlad referencia na panel spravcu lig
     */
    public SpravcaLigKontroler(HlavnyModel paModel, SpravcaLigPanel paPohlad) {
        aModel = paModel;
        aSpravcaLigPohlad = paPohlad;
        aHlavnyKontroler = HlavnyKontroler.dajInstanciu();

        aSpravcaLigPohlad.addVlozitTimListener(new VlozitTimListener());
        aSpravcaLigPohlad.addUpravitTimListener(new UpravitTimListener());
        aSpravcaLigPohlad.addVymazatTimListener(new VymazatTimListener());
        aSpravcaLigPohlad.addVlozitLiguListener(new VlozitLiguListener());
        aSpravcaLigPohlad.addUpravitLiguListener(new UpravitLiguListener());
        aSpravcaLigPohlad.addVymazatLiguListener(new VymazatLiguListener());
    }
    
    /**
     * Metoda pre obnovenie udajov v paneli spravca lig
     */
    @Override
    public void obnovUdaje() {
        aSpravcaLigPohlad.vymazTabulkuLig();
        aSpravcaLigPohlad.vymazTabulkuTimov();
                
        for (int i = 0; i < aModel.dajPocetTimov(); i++) {
            aSpravcaLigPohlad.vlozDoTabulkyTimov(i+1, aModel.dajTim(i));
        }
        for (int i = 0; i < aModel.dajPocetLig(); i++) {
            aSpravcaLigPohlad.vlozDoTabulkyLig(i+1, aModel.dajLigu(i));
        }
    }
   
    class VlozitTimListener implements ActionListener {
        @Override
        public void actionPerformed(ActionEvent e) {
            String nazovTimu = aSpravcaLigPohlad.dajNazovTimu().replace(",", " ");
            if(nazovTimu.length() > 0) {
                aModel.vlozTim(nazovTimu);
                aHlavnyKontroler.obnovUdaje();
            }
        }
    }
    
    class UpravitTimListener implements ActionListener {
        @Override
        public void actionPerformed(ActionEvent e) {
            String nazovTimu = aSpravcaLigPohlad.dajNazovTimu().replace(",", " ");
            if(aSpravcaLigPohlad.dajTim() != null && nazovTimu.length() > 0) {
                aModel.upravTim((Tim) aSpravcaLigPohlad.dajTim(), nazovTimu);
                aHlavnyKontroler.obnovUdaje();
            }
        }
    }
    
    class VymazatTimListener implements ActionListener {
        @Override
        public void actionPerformed(ActionEvent e) {
            if(aSpravcaLigPohlad.dajTim() != null) {
                aModel.vymazTim((Tim) aSpravcaLigPohlad.dajTim());
                aHlavnyKontroler.obnovUdaje();
            }
        }
    }
    
    class VlozitLiguListener implements ActionListener {
        @Override
        public void actionPerformed(ActionEvent e) {
            String nazovLigy = aSpravcaLigPohlad.dajNazovLigy().replace(",", " ");
            if(nazovLigy.length() > 0) {
                aModel.vlozLigu(nazovLigy);
                aHlavnyKontroler.obnovZoznamLig();
            }
        }
    }
    
    class UpravitLiguListener implements ActionListener {
        @Override
        public void actionPerformed(ActionEvent e) {
            String nazovLigy = aSpravcaLigPohlad.dajNazovLigy().replace(",", " ");
            if(aSpravcaLigPohlad.dajLigu() != null && nazovLigy.length() > 0) {
                aModel.upravLigu((Liga) aSpravcaLigPohlad.dajLigu(), nazovLigy);
                aHlavnyKontroler.obnovZoznamLig();
            }
        }
    }
    
    class VymazatLiguListener implements ActionListener {
        @Override
        public void actionPerformed(ActionEvent e) {
            if(aSpravcaLigPohlad.dajLigu() != null) {
                aModel.vymazLigu((Liga) aSpravcaLigPohlad.dajLigu());
                aHlavnyKontroler.obnovZoznamLig();
            }
        }
    }
}
