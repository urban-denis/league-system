package kontrolery;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.util.Iterator;
import logika.*;
import modely.HlavnyModel;
import pohlady.SpravcaZapasovPanel;

/**
 * Kontroler pre panel spravca zapasov
 * Komunikuje s hl. modelom a vykonava vsetky akcie spojene so spravcom zapasov
 * @author Denis
 */
public final class SpravcaZapasovKontroler implements IKontroler {

    private final HlavnyModel aModel;
    private final SpravcaZapasovPanel aSpravcaZapasovPohlad;
    private final HlavnyKontroler aHlavnyKontroler;

    /**
     * Konstruktor vytvara kontroler s referenciami na jeho model a pohlad
     * Vytvara jednotlive listenery pre akcie tlacidiel
     * @param paModel referencia na hl. model
     * @param paPohlad referencia na hl. pohlad
     */
    public SpravcaZapasovKontroler(HlavnyModel paModel, SpravcaZapasovPanel paPohlad) {
        aModel = paModel;
        aSpravcaZapasovPohlad = paPohlad;
        aHlavnyKontroler = HlavnyKontroler.dajInstanciu();

        aSpravcaZapasovPohlad.addTabulkaMouseClicked(new TabulkaClickedListener());
        aSpravcaZapasovPohlad.addVlozitZapasListener(new VlozitZapasListener());
        aSpravcaZapasovPohlad.addUpravitZapasListener(new UpravitZapasListener());
        aSpravcaZapasovPohlad.addVymazatZapasListener(new VymazatZapasListener());
    }

    /**
     * Metoda pre obnovenie udajov v paneli spravca zapasov
     */
    @Override
    public void obnovUdaje() {
        aSpravcaZapasovPohlad.resetujVstupy();

        Iterator<Tim> timy = aModel.dajTimy();
        Iterator<Kolo> kola = aModel.dajKola();

        while (timy.hasNext()) {
            Tim tim = timy.next();
            aSpravcaZapasovPohlad.vlozDoZoznamov(tim.toString(), tim);
        }

        while (kola.hasNext()) {
            Kolo kolo = kola.next();
            for (int i = 0; i < kolo.dajPocetZapasov(); i++) {
                Zapas zapas = kolo.dajZapas(i);
                aSpravcaZapasovPohlad.vlozDoTabulky(kolo, zapas);
            }
        }
    }

    class TabulkaClickedListener implements MouseListener {

        @Override
        public void mouseClicked(MouseEvent e) {
            Zapas zapas = (Zapas) aSpravcaZapasovPohlad.dajZapas();
            if (zapas != null) {
                aSpravcaZapasovPohlad.nastavTimDomaci(zapas.dajDomacich());
                aSpravcaZapasovPohlad.nastavTimHostia(zapas.dajHosta());
                aSpravcaZapasovPohlad.nastavSkoreDomacich(zapas.dajSkoreDomacich());
                aSpravcaZapasovPohlad.nastavSkoreHosti(zapas.dajSkoreHosti());
                aSpravcaZapasovPohlad.nastavKolo(aModel.dajCisloKolaZapasu(zapas));
            }
        }

        @Override
        public void mousePressed(MouseEvent e) {
        }

        @Override
        public void mouseReleased(MouseEvent e) {
        }

        @Override
        public void mouseEntered(MouseEvent e) {
        }

        @Override
        public void mouseExited(MouseEvent e) {
        }
    }

    class VlozitZapasListener implements ActionListener {

        @Override
        public void actionPerformed(ActionEvent e) {
            Tim timDomaci = (Tim) aSpravcaZapasovPohlad.dajDomacich();
            Tim timHostia = (Tim) aSpravcaZapasovPohlad.dajHosti();

            int cisloKola = aSpravcaZapasovPohlad.dajCisloKola();

            String skoreDomaci = aSpravcaZapasovPohlad.dajSkoreDomacich();
            String skoreHostia = aSpravcaZapasovPohlad.dajSkoreHosti();

            if (jeSpravneSkore(skoreDomaci, skoreHostia) && jeSpravneKolo(cisloKola) && !timDomaci.equals(timHostia)) {
                if (timDomaci != null && timHostia != null && cisloKola > 0) {
                    Zapas zapas = new Zapas(timDomaci, timHostia,
                            Integer.parseInt(skoreDomaci),
                            Integer.parseInt(skoreHostia));

                    aModel.vlozZapas(cisloKola, zapas);
                    aHlavnyKontroler.obnovUdaje();
                }
            }
        }
    }

    class UpravitZapasListener implements ActionListener {

        @Override
        public void actionPerformed(ActionEvent e) {
            Zapas zapas = (Zapas) aSpravcaZapasovPohlad.dajZapas();
            Tim timDomaci = (Tim) aSpravcaZapasovPohlad.dajDomacich();
            Tim timHostia = (Tim) aSpravcaZapasovPohlad.dajHosti();

            int cisloKola = aSpravcaZapasovPohlad.dajCisloKola();

            String skoreDomaci = aSpravcaZapasovPohlad.dajSkoreDomacich();
            String skoreHostia = aSpravcaZapasovPohlad.dajSkoreHosti();

            if (zapas != null && jeSpravneSkore(skoreDomaci, skoreHostia) && jeSpravneKolo(cisloKola) && !timDomaci.equals(timHostia)) {
                aModel.upravZapas(cisloKola, zapas, timDomaci, timHostia, Integer.parseInt(skoreDomaci), Integer.parseInt(skoreHostia));
                aHlavnyKontroler.obnovUdaje();
            }
        }
    }

    class VymazatZapasListener implements ActionListener {

        @Override
        public void actionPerformed(ActionEvent e) {
            Zapas zapas = (Zapas) aSpravcaZapasovPohlad.dajZapas();
            Kolo kolo = (Kolo) aSpravcaZapasovPohlad.dajKolo();

            if (kolo != null && zapas != null) {
                aModel.vymazZapas(kolo, zapas);
                aHlavnyKontroler.obnovUdaje();
            }
        }
    }

    private boolean jeNumericke(String paCislo) {
        return paCislo.matches("[-+]?\\d*\\.?\\d+");
    }

    private boolean jeSpravneSkore(String paDomaci, String paHostia) {
        if (jeNumericke(paDomaci) && jeNumericke(paHostia)) {
            int sucetSkore = 18;
            return (Integer.parseInt(paDomaci) + Integer.parseInt(paHostia)) == sucetSkore;
        }

        return false;
    }

    private boolean jeSpravneKolo(int paKolo) {
        int maxKolo = 30;
        return paKolo > 0 && paKolo <= maxKolo;
    }
}
