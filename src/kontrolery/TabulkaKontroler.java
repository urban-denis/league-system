package kontrolery;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import modely.TabulkaModel;
import pohlady.TabulkaPanel;

/**
 * Kontroler pre panel tabulka
 * Komunikuje s modelom tabulka a vykonava vsetky akcie spojene s tabulkou
 * @author Denis
 */
public final class TabulkaKontroler implements IKontroler {
    private final TabulkaModel aTabulkaModel;
    private final TabulkaPanel aTabulkaPohlad;
    
    /**
     * Konstruktor vytvara kontroler s referenciami na jeho model a pohlad
     * Vytvara jednotlive listenery pre akcie tlacidiel
     * @param paModel referencia na TabulkaModel
     * @param paPohlad referencia na TabulkaPanel
     */
    public TabulkaKontroler(TabulkaModel paModel, TabulkaPanel paPohlad) {
        aTabulkaModel = paModel;
        aTabulkaPohlad = paPohlad;
        
        aTabulkaPohlad.addDalsieListener(new DalsieListener());
        aTabulkaPohlad.addPredosleListener(new PredosleListener());
    }

    /**
     * Metoda pre obnovenie udajov v paneli tabulka
     */
    @Override
    public void obnovUdaje() {
        aTabulkaPohlad.vymazTabulku();
        if(aTabulkaModel.dajKolo() != null) {
            String[][] zoradeneTimy = aTabulkaModel.dajZoradeneTimy();
            for(int i = 0; i < zoradeneTimy.length; i++) {
                aTabulkaPohlad.vlozDoTabulky(i+1, zoradeneTimy[i][1], zoradeneTimy[i][2], zoradeneTimy[i][3], zoradeneTimy[i][4], zoradeneTimy[i][5], zoradeneTimy[i][6], zoradeneTimy[i][7]);
            }
        }
        
        this.obnovKolo();
    }
    
    private void obnovKolo() {
        if (aTabulkaModel.maPredosleKolo()) {
            aTabulkaPohlad.ukazPredosle();
        } else {
            aTabulkaPohlad.skryPredosle();
        }

        if (aTabulkaModel.maDalsieKolo()) {
            aTabulkaPohlad.ukazDalsie();
        } else {
            aTabulkaPohlad.skryDalsie();
        }
        
        if(aTabulkaModel.dajKolo() != null) {
            aTabulkaPohlad.nastavKolo(aTabulkaModel.dajKolo().toString());
        } else {
            aTabulkaPohlad.nastavKolo("");
        }
    }
    
    class DalsieListener implements ActionListener{
        @Override
        public void actionPerformed(ActionEvent e) {
            aTabulkaModel.dajDalsieKolo();
            obnovUdaje();
        }
    }
    
    class PredosleListener implements ActionListener{
        @Override
        public void actionPerformed(ActionEvent e) {
            aTabulkaModel.dajPredosleKolo();
            obnovUdaje();
        }
    }
}
