package kontrolery;

/**
 * Interface pre vsetky kontrolery
 * @author Denis
 */
public interface IKontroler {

    /**
     * obnovi udaje v paneloch
     */
    void obnovUdaje();
}
