package kontrolery;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.util.ArrayList;
import logika.Liga;
import pohlady.PrvokZoznamu;
import modely.*;
import pohlady.*;

/**
 * Hlavny kontroler obsahuje zoznam vsetkych kontrolerov
 * Komunikuje s hl. modelom a s hl. pohladom
 * Reaguje na zmenu ligy
 * 
 * @author Denis
 */

public final class HlavnyKontroler implements IKontroler {
    private final HlavnyPohlad aPohlad;
    private final HlavnyModel aModel;
    private static HlavnyKontroler hlavnyKontroler;
    
    private final ArrayList<IKontroler> aKontrolery;
    
    /**
     * Singleton konstruktor pre hl. kontroler
     * @return HlavnyKontroler
     */
    public static synchronized HlavnyKontroler dajInstanciu() {
        if(hlavnyKontroler == null) {
            hlavnyKontroler = new HlavnyKontroler();
        }
        return hlavnyKontroler;
    }
    
    private HlavnyKontroler() {
        aPohlad = new HlavnyPohlad();
        aModel = new HlavnyModel();
        aKontrolery = new ArrayList<>();
        
        aPohlad.zmenaLigyListener(new ZmenaLigyListener());
        aPohlad.addUlozitAktivituListener(new UlozitAktivituListener());
    }
    
    /**
    * Inicializuje vsetky kontrolery s ich modelmi a pohladmi
    */
    public void vytvorKontrolery() {
        aKontrolery.add(new TabulkaKontroler(new TabulkaModel(aModel), new TabulkaPanel(aPohlad)));
        aKontrolery.add(new SpravcaLigKontroler(aModel, new SpravcaLigPanel(aPohlad)));
        aKontrolery.add(new SpravcaZapasovKontroler(aModel, new SpravcaZapasovPanel(aPohlad)));
        this.obnovZoznamLig();
        aPohlad.zobraz();
    }
    
    /**
     * Metoda pre obnovenie udajov vsetkych kontrolerov - ich panelov
     * Ak sa vykona akcia v niektorom kontroleri, tak sa spusti tato metoda
     */
    @Override
    public void obnovUdaje() {
        for(IKontroler kontroler : aKontrolery) {
            kontroler.obnovUdaje();
        }
    }
    
    /**
     * Metoda pre obnovenie zoznamu lig
     */
    public void obnovZoznamLig() {
        aPohlad.vymazZoznamLig();
        for(int i = 0; i < aModel.dajPocetLig(); i++) {
            aPohlad.vlozDoZoznamuLig(aModel.dajLigu(i).toString(), aModel.dajLigu(i));
        }
    }
    
    class ZmenaLigyListener implements ItemListener {
        @Override
        public void itemStateChanged(ItemEvent e) {
            if(aPohlad.suVZoznameLig()) {
                Liga liga = (Liga) ((PrvokZoznamu) aPohlad.dajVybranuLigu()).dajHodnotu();
                aModel.zmenLigu(liga);
                obnovUdaje();
            }
        }
    }
    
    class UlozitAktivituListener implements ActionListener {
        @Override
        public void actionPerformed(ActionEvent e) {
            aModel.ulozAktivitu();
        }
    }
}