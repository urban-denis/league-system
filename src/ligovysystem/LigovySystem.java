package ligovysystem;

import kontrolery.HlavnyKontroler;

/**
 * 
 * @author Denis
 */
public class LigovySystem {

    /**
     * @param args argumenty
     */
    public static void main(String[] args) {
        HlavnyKontroler.dajInstanciu().vytvorKontrolery();
    }
}
