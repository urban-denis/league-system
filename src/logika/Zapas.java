package logika;

/**
 * Trieda pre zapasy medzi dvomi supermi
 * @author Denis
 */
public class Zapas {
    private Tim aDomaci;
    private Tim aHostia;
    private int aSkoreDomaci;
    private int aSkoreHostia;
    
    /**
     * Konstruktor vytvara zapas
     * @param paDomaci domaci tim
     * @param paHostia tim hosti
     * @param paSkoreDomaci skore domacich
     * @param paSkoreHostia skore hosti
     */
    public Zapas(Tim paDomaci, Tim paHostia, int paSkoreDomaci, int paSkoreHostia) {
        aDomaci = paDomaci;
        aHostia = paHostia;
        aSkoreDomaci = paSkoreDomaci;
        aSkoreHostia = paSkoreHostia;
        //aCasKonania = paCasKonania;
    }
    
    /**
     * Metoda vracia vitaza
     * @return vitazny tim
     */
    public Tim dajVitaza() {
        if(aSkoreDomaci > aSkoreHostia) {
            return aDomaci;
        } else {
            return aHostia;
        }
    }
    
    /**
     * Metoda pre overenie ci v zapase hral zadany tim
     * @param paTim tim
     * @return true/false
     */
    public boolean jeTim(Tim paTim) {
        return aDomaci.equals(paTim) || aHostia.equals(paTim);
    }
    
    /**
     * Metoda vracia true ak bola remiza
     * @return true/false
     */
    public boolean jeRemiza() {
        return aSkoreDomaci == aSkoreHostia;
    }
    
    /**
     * Metoda vracia skore hosti
     * @return skore hosti
     */
    public int dajSkoreHosti() {
        return aSkoreHostia;
    }
    
    /**
     * Metoda vracia skore domacich
     * @return skore domacich
     */
    public int dajSkoreDomacich() {
        return aSkoreDomaci;
    }
    
    /**
     * Metoda vracia skore podla zadaneho timu
     * @param paTim tim
     * @return skore timu
     */
    public int dajSkoreTimu(Tim paTim) {
        if(paTim.equals(aDomaci)) {
            return aSkoreDomaci;
        } else {
            return aSkoreHostia;
        }
    }
    
    /**
     * Metoda vracia domaci tim
     * @return domaci tim
     */
    public Tim dajDomacich() {
        return aDomaci;
    }
    
    /**
     * Metoda vracia tim hosti
     * @return tim hosti
     */
    public Tim dajHosta() {
        return aHostia;
    }
    
    /**
     * Metoda pre upravu zapasu
     * @param paDomaci domaci tim
     * @param paHostia tim hosti
     * @param paSkoreDomaci skore domacich
     * @param paSkoreHostia skore hosti
     */
    public void zmenZapas(Tim paDomaci, Tim paHostia, int paSkoreDomaci, int paSkoreHostia) {
        aDomaci = paDomaci;
        aHostia = paHostia;
        aSkoreDomaci = paSkoreDomaci;
        aSkoreHostia = paSkoreHostia;
    }
    
    @Override
    public String toString() {
        return aDomaci.toString() + " " + aSkoreDomaci + " : " + aSkoreHostia + " " + aHostia.toString();
    }
}