package logika;

import java.util.ArrayList;

/**
 * Trieda pre kola
 * Kazde kolo ma svoje cislo
 * @author Denis
 */
public class Kolo {
    private final int aCislo;
    private final ArrayList<Zapas> aZapasy;
    
    /**
     * Vytvory kolo a nastavi mu cislo
     * @param paCislo
     */
    public Kolo(int paCislo) {
        aCislo = paCislo;
        aZapasy = new ArrayList<>();
    }
    
    /**
     * Vlozi zapas do daneho kola
     * @param paZapas zapas
     */
    public void vlozZapas(Zapas paZapas) {
        if (paZapas != null) {
            aZapasy.add(paZapas);
        }
    }
    
    /**
     * Vracia true ak v kole nie su ziadne zapasy
     * @return true/false
     */
    public boolean jePrazdne() {
        return aZapasy.isEmpty();
    }
    
    /**
     * Vracia pole statistik v danom kole podla zadaneho timu
     * @param paTim tim
     * @return pole statistik
     */
    public int[] dajPocetZVPRB(Tim paTim) {
        int[] pocetZVPRB = new int[5];
        int pocetZapasov = 0, pocetPrehier = 0, pocetVyhier = 0, pocetRemiz = 0, pocetBodov = 0;
        for(Zapas zapas : aZapasy) {
            if(zapas.jeTim(paTim)) {
                pocetZapasov++;
                if(zapas.jeRemiza()) {
                    pocetRemiz++;
                    pocetBodov += 2;
                } else if(zapas.dajVitaza().equals(paTim)) {
                    pocetVyhier++;
                    pocetBodov += 3;
                } else {
                    pocetPrehier++;
                    pocetBodov++;
                }
            }
        }
        
        pocetZVPRB[0] = pocetZapasov;
        pocetZVPRB[1] = pocetVyhier;
        pocetZVPRB[2] = pocetPrehier;
        pocetZVPRB[3] = pocetRemiz;
        pocetZVPRB[4] = pocetBodov;
        
        return pocetZVPRB;
    }
    
    /**
     * Vracia skore daneho timu v danom kole
     * @param paTim tim
     * @return pole so ziskanimi a stratenimi bodmi
     */
    public int[] dajSkore(Tim paTim) {
        int ziskane = 0, stratene = 0;
        int[] skore = new int[2];
        for(Zapas zapas : aZapasy) {
            if(zapas.jeTim(paTim)) {
                ziskane = zapas.dajSkoreTimu(paTim);
                stratene = 18 - ziskane;
            }
        }
        skore[0] = ziskane;
        skore[1] = stratene;
        return skore;
    }
    
    /**
     * Vrati cislo kola
     * @return cislo kola
     */
    public int dajCislo() {
        return aCislo;
    }
    
    /**
     * Vrati true ak kolo obsahuje zadany zapas
     * @param paZapas zapas
     * @return true/false
     */
    public boolean masZapas(Zapas paZapas) {
        return aZapasy.contains(paZapas);
    }
    
    /**
     * Vrati pocet zapasov v kole
     * @return pocet zapasov
     */
    public int dajPocetZapasov() {
        return aZapasy.size();
    }
    
    /**
     * Vrati zapas podla indexu
     * @param paIndex index zapasu
     * @return zapas
     */
    public Zapas dajZapas(int paIndex) {
        return aZapasy.get(paIndex);
    }
    
    /**
     * Vymaze zapas zo zoznamu
     * @param paZapas zapas
     */
    public void vymazZapas(Zapas paZapas) {
        aZapasy.remove(paZapas);
    }
    
    @Override
    public String toString() {
        return "Kolo: " + aCislo;
    }
}
