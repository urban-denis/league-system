package logika;

/**
 * Trieda pre timy, ktore potom ukladame do jednotlivych lig
 * @author Denis
 */
public class Tim {
    private String aNazov;
    
    /**
     * Konstruktor pre tim
     * @param paNazov - nazov timu
     */
    public Tim(String paNazov) {
        aNazov = paNazov;
    }
    
    /**
     * Metoda pre zmenu nazvu timu
     * @param paNazov
     */
    public void zmenitNazov(String paNazov) {
        aNazov = paNazov;
    }
    
    @Override
    public String toString() {
        return aNazov;
    }
}
