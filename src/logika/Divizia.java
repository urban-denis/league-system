package logika;

import java.io.File;
import java.util.ArrayList;
import java.util.Iterator;
import subory.Zapisovac;

/**
 * V triede Divizia su ulozene vsetky ligy
 * Sluzi ako hl. wrapper pre cely system
 * 
 * @author Denis
 */
public class Divizia {

    private final ArrayList<Liga> aZoznamLig;

    /**
     * Kontstruktor pre triedu Divizia
     */
    public Divizia() {
        aZoznamLig = new ArrayList<>();
    }

    /**
     * Metoda pre pridanie novej ligy do systemu
     * @param paLiga - liga
     */
    public void pridajLigu(Liga paLiga) {
        if(paLiga != null) {
            aZoznamLig.add(paLiga);
        }
    }

    /**
     * Metoda pre vymazanie ligy zo zoznamu
     * @param paLiga - liga
     */
    public void vymazLigu(Liga paLiga) {
        aZoznamLig.remove(paLiga);
    }

    /**
     * Metoda vrati index ligy
     * @param paLiga - liga
     * @return - cislo ligy v zozname
     */
    public int dajIndexLigy(Liga paLiga) {
        return aZoznamLig.indexOf(paLiga);
    }

    /**
     * Metoda vrati ligu podla cisla v zozname
     * @param paIndex - index ligy
     * @return - liga
     */
    public Liga dajLigu(int paIndex) {
        return aZoznamLig.get(paIndex);
    }

    /**
     * Metoda vracia pocet lig v zozname
     * @return - pocet lig
     */
    public int dajPocetLig() {
        return aZoznamLig.size();
    }
    
    public void zapisUdaje() {
        this.zapisLig();
        this.zapisTimov();
        this.zapisZapasov();
    }

    /**
     * Metoda pre zapis vsetkych lig do suboru
     * Subor obsahuje len nazov ligy
     */
    private void zapisLig() {
        Zapisovac zapisovac = new Zapisovac(new File("data/ligy.txt"));
        for (Liga liga : aZoznamLig) {
            zapisovac.zapis(liga.toString());
        }
        zapisovac.zatvor();
    }

    /**
     * Metoda pre zapis vsetkych timov do suboru
     * Subor obsahuje nazov timu a cislo ligy do ktorej patri
     */
    private void zapisTimov() {
        Zapisovac zapisovac = new Zapisovac(new File("data/timy.txt"));
        for (Liga liga : aZoznamLig) {
            for (int i = 0; i < liga.dajPocetTimov(); i++) {
                zapisovac.zapis(liga.dajTim(i).toString() + "," + aZoznamLig.indexOf(liga) + ",");
            }
        }
        zapisovac.zatvor();
    }

    /**
     * Metoda pre zapis vsetkych zapasov do suboru
     * Jeden riadok suboru znamena jeden zapas
     * Riadok obsahuje cislo ligy, do ktorej patri zapas, cislo kola v ktorom bol zapas odohrany, index domaceho a hostujuceho timu, skore domacich a skore hosti
     */
    private void zapisZapasov() {
        Zapisovac zapisovac = new Zapisovac(new File("data/zapasy.txt"));
        for (Liga liga : aZoznamLig) {
            Iterator<Kolo> kola = liga.dajKolaIterator();
            while (kola.hasNext()) {
                Kolo kolo = kola.next();
                for (int i = 0; i < kolo.dajPocetZapasov(); i++) {
                    int domaci = liga.dajIndexTimu(kolo.dajZapas(i).dajDomacich());
                    int hostia = liga.dajIndexTimu(kolo.dajZapas(i).dajHosta());
                    zapisovac.zapis(aZoznamLig.indexOf(liga) + "," + kolo.dajCislo() + "," + domaci + "," + hostia + "," + kolo.dajZapas(i).dajSkoreDomacich() + "," + kolo.dajZapas(i).dajSkoreHosti() + ",");
                }
            }
        }
        zapisovac.zatvor();
    }
}
