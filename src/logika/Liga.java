package logika;

import java.util.ArrayList;
import java.util.Iterator;

/**
 * Trieda pre ligy
 * @author Denis
 */
public class Liga {
    private String aNazov;
    private final ArrayList<Tim> aZoznamTimov;
    private final ArrayList<Kolo> aZoznamKol;
    
    /**
     * Vytvori ligu
     * @param paNazov nazov ligy
     */
    public Liga(String paNazov) {
        aNazov = paNazov;
        aZoznamTimov = new ArrayList<>();
        aZoznamKol = new ArrayList<>();
    }
    
    /**
     * Zmeni nazov ligy
     * @param paNazov nazov ligy
     */
    public void zmenitNazov(String paNazov) {
        aNazov = paNazov;
    }
    
    /**
     * Vlozi novy tim do ligy
     * @param paTim tim
     */
    public void vlozTim(Tim paTim) {
        aZoznamTimov.add(paTim);
    }
    
    /**
     * Odstrani tim z ligy
     * @param paTim tim
     */
    public void vymazTim(Tim paTim) {
        aZoznamTimov.remove(paTim);
    }
    
    /**
     * Vlozi kolo do ligy
     * @param paKolo kolo
     */
    public void vlozKolo(Kolo paKolo) {
        aZoznamKol.add(paKolo);
    }
    
    /**
     * Vrati tim podla zadaneho indexu
     * @param paIndex index
     * @return tim
     */
    public Tim dajTim(int paIndex) {
        return aZoznamTimov.get(paIndex);
    }
    
    /**
     * Vrati pocet timov
     * @return pocet timov
     */
    public int dajPocetTimov() {
        return aZoznamTimov.size();
    }
    
    /**
     * Vrati pocet kol
     * @return pocet kol
     */
    public int dajPocetKol() {
        return aZoznamKol.size();
    }
        
    /**
     * Vrati najaktualnejsie kolo - kolo s najvacsim cislom
     * @return kolo
     */
    public Kolo dajMaxKolo() {
        if(aZoznamKol.isEmpty()) {
            return null;
        }
        
        int maximum = 1;
        for(Kolo kolo : aZoznamKol) {
            if(kolo.dajCislo() > maximum) {
                maximum = kolo.dajCislo();
            }
        }
        return this.dajKolo(maximum);
    }
    
    /**
     * Vlozi zapas do ligy
     * @param paCisloKola cislo kola
     * @param paZapas zapas
     */
    public void vlozZapas(int paCisloKola, Zapas paZapas) {
        if(this.jeKolo(paCisloKola)) {
            Kolo kolo = this.dajKolo(paCisloKola);
            kolo.vlozZapas(paZapas);
        } else {
            Kolo kolo = new Kolo(paCisloKola);
            kolo.vlozZapas(paZapas);
            this.vlozKolo(kolo);
        }
    }
    
    /**
     * Vrati kolo podla cisla kola
     * @param paCisloKola cislo kola
     * @return kolo
     */
    public Kolo dajKolo(int paCisloKola) {
        for(Kolo kolo : aZoznamKol) {
            if(kolo.dajCislo() == paCisloKola) {
                return kolo;
            }
        }
        return null;
    }
    
    /**
     * Vrati vyssie kolo od zadaneho kola
     * @param paKolo kolo
     * @return kolo
     */
    public Kolo dajDalsieKolo(Kolo paKolo) {
        if(aZoznamKol.isEmpty()) {
            return null;
        }
        
        int maxRozdiel = this.dajMaxKolo().dajCislo();
        int rozdiel = 0;
        int cisloKola = paKolo.dajCislo();
        
        if(cisloKola >= maxRozdiel) {
            return null;
        }
        
        Kolo minKolo = null;
        for(Kolo kolo : aZoznamKol) {
            rozdiel = kolo.dajCislo() - cisloKola;
            if(rozdiel > 0 && rozdiel < maxRozdiel) {
                maxRozdiel = rozdiel;
                minKolo = kolo;
            }
        }
        return minKolo;
    }
    
    /**
     * Vrati nizsie kolo od zadaneho kola
     * @param paKolo kolo
     * @return kolo
     */
    public Kolo dajPredosleKolo(Kolo paKolo) {
        if(aZoznamKol.isEmpty()) {
            return null;
        }
        
        int cisloKola = paKolo.dajCislo();
        
        if(cisloKola <= 1) {
            return null;
        }
        
        int maxRozdiel = dajMaxKolo().dajCislo();
        int rozdiel = 0;
        
        Kolo minKolo = null;
        for(Kolo kolo : aZoznamKol) {
            rozdiel = cisloKola - kolo.dajCislo();
            if(rozdiel > 0 && rozdiel < maxRozdiel) {
                maxRozdiel = rozdiel;
                minKolo = kolo;
            }
        }
        return minKolo;
    }
    
    /**
     * Vrati true ak existuje kolo so zadanym cislom
     * @param paCisloKola cislo kola
     * @return true/false
     */
    public boolean jeKolo(int paCisloKola) {
        for(Kolo kolo : aZoznamKol) {
            return kolo.dajCislo() == paCisloKola;
        }
        return false;
    }
    
    /**
     * Vrati pole statistik timu do konkretneho kola
     * @param paTim tim
     * @param paKolo kolo
     * @return pole statistik | pocet zapasov, pocet vyhier, prehier, remiz a celk. bodov
     */
    public int[] dajPocetZVPRB(Tim paTim, Kolo paKolo) {
        int[] pocetZVPRB = new int[5];
        int pocetZapasov = 0, pocetPrehier = 0, pocetVyhier = 0, pocetRemiz = 0, pocetBodov = 0;
        for(Kolo kolo : aZoznamKol) {
            if(kolo.dajCislo() <= paKolo.dajCislo()) {
                pocetZapasov += kolo.dajPocetZVPRB(paTim)[0];
                pocetVyhier += kolo.dajPocetZVPRB(paTim)[1];
                pocetPrehier += kolo.dajPocetZVPRB(paTim)[2];
                pocetRemiz += kolo.dajPocetZVPRB(paTim)[3];
                pocetBodov += kolo.dajPocetZVPRB(paTim)[4];
            }
        }
        
        pocetZVPRB[0] = pocetZapasov;
        pocetZVPRB[1] = pocetVyhier;
        pocetZVPRB[2] = pocetPrehier;
        pocetZVPRB[3] = pocetRemiz;
        pocetZVPRB[4] = pocetBodov;
        
        return pocetZVPRB;
    }
    
    /**
     * Vrati celkove skore
     * @param paTim tim
     * @param paKolo kolo
     * @return celkove skore | ziskane body : stratene body
     */
    public String dajCelkoveSkore(Tim paTim, Kolo paKolo) {
        int ziskaneBody = 0;
        int strateneBody = 0;
        for(Kolo kolo : aZoznamKol) {
            if(kolo.dajCislo() <= paKolo.dajCislo()) {
                ziskaneBody += kolo.dajSkore(paTim)[0];
                strateneBody += kolo.dajSkore(paTim)[1];
            }
        }
        return "" + ziskaneBody + " : " + strateneBody;
    }
    
    /**
     * Vrati kolo podla zadaneho zapasu
     * @param paZapas zapas
     * @return kolo
     */
    public Kolo dajKoloZapasu(Zapas paZapas) {
        for(Kolo kolo : aZoznamKol) {
            if(kolo.masZapas(paZapas)) {
                return kolo;
            }
        }
        return null;
    }
    
    /**
     * Prehodi zapas do druheho kola
     * @param paCisloKola cislo kola
     * @param paZapas zapas
     */
    public void nahradZapas(int paCisloKola, Zapas paZapas) {
        Kolo kolo = this.dajKoloZapasu(paZapas);
        kolo.vymazZapas(paZapas);
        if(kolo.jePrazdne()) {
            aZoznamKol.remove(kolo);
        }
        this.vlozZapas(paCisloKola, paZapas);
    }
    
    /**
     * Vrati index podla zadaneho timu
     * @param paTim tim
     * @return index timu
     */
    public int dajIndexTimu(Tim paTim) {
        if(paTim != null) {
            return aZoznamTimov.indexOf(paTim);
        }
        
        return -1;
    }
    
    /**
     * Vrati iterator tim
     * @return iterator tim
     */
    public Iterator<Tim> dajTimyIterator() {
        return aZoznamTimov.iterator();
    }
    
    /**
     * Vrati iterator kolo
     * @return iterator kolo
     */
    public Iterator<Kolo> dajKolaIterator() {
        return aZoznamKol.iterator();
    }
    
    @Override
    public String toString() {
        return aNazov;
    }
}
