package modely;

import java.beans.PropertyChangeListener;
import java.beans.PropertyChangeSupport;

/**
 * Rodicovska trieda pre modely
 * Ma za ulohu monitorovanie zmien atributov
 * @author Denis
 */
public abstract class Model {
    protected PropertyChangeSupport propertyChangeSupport;

    /**
     * Vytvorenie podpory pre zmeny
     */
    public Model() {
        propertyChangeSupport = new PropertyChangeSupport(this);
    }

    /**
     * Pridanie listenera
     * @param paListener property change paListener
     */
    public void pridajListener(PropertyChangeListener paListener) {
        propertyChangeSupport.addPropertyChangeListener(paListener);
    }

    /**
     * Vymazanie listenera
     * @param paListener property change paListener
     */
    public void odstranListener(PropertyChangeListener paListener) {
        propertyChangeSupport.removePropertyChangeListener(paListener);
    }

    /**
     * Zaznamenanie zmeny atributu
     * @param paNazovAtr nazov atributu
     * @param paStaraHodnota stara hodnota
     * @param paNovaHodnota nova hodnota
     */
    protected void firePropertyChange(String paNazovAtr, Object paStaraHodnota,
            Object paNovaHodnota) {
        propertyChangeSupport.firePropertyChange(paNazovAtr, paStaraHodnota,
                paNovaHodnota);
    }
}
