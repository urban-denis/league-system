package modely;

import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import logika.*;

/**
 * Model pre panel tabulka
 * Obsahuje vsetky operacie s datami spojene s ligovou tabulkou
 * @author Denis
 */
public final class TabulkaModel extends Model implements PropertyChangeListener {

    private Kolo aKolo;
    private final HlavnyModel aHlavnyModel;

    /**
     * Vytvori model tabulky
     * @param paModel referencia na hl. model
     */
    public TabulkaModel(HlavnyModel paModel) {
        aHlavnyModel = paModel;
        aHlavnyModel.pridajListener(this);
        aKolo = aHlavnyModel.dajMaxKolo();
    }

    /**
     * Vrati aktualne kolo
     * @return kolo
     */
    public Kolo dajKolo() {
        return aKolo;
    }

    /**
     * Vrati celkovy pocet timov v aktualnej lige
     * @return pocet timov
     */
    public int dajPocetTimov() {
        if(aHlavnyModel.dajPocetLig() < 1) {
            return 0;
        }
        return aHlavnyModel.dajLigu().dajPocetTimov();
    }

    /**
     * Resetuje kolo na najaktualnejsie kolo
     */
    public void resetujKolo() {
        aKolo = aHlavnyModel.dajMaxKolo();
    }

    /**
     * Vrati udaje tabulkove udaje timu, ktore su ulozene v poli
     * @param paCisloTimu index timu
     * @return pole udajov timu
     */
    public String[] dajRiadokTimu(int paCisloTimu) {
        String[] riadok = new String[8];
        if (aKolo == null) {
            return riadok;
        }

        Tim tim = aHlavnyModel.dajLigu().dajTim(paCisloTimu);
        int[] ZVPRB = aHlavnyModel.dajLigu().dajPocetZVPRB(tim, aKolo);
        riadok[0] = "" + paCisloTimu + 1;
        riadok[1] = tim.toString();
        riadok[2] = "" + ZVPRB[0];
        riadok[3] = "" + ZVPRB[1];
        riadok[4] = "" + ZVPRB[2];
        riadok[5] = "" + ZVPRB[3];
        riadok[6] = aHlavnyModel.dajLigu().dajCelkoveSkore(tim, aKolo);
        riadok[7] = "" + ZVPRB[4];

        return riadok;
    }

    /**
     * Vrati bodovo zoradene timy a ich udaje ulozene v dvojrozmernom poli
     * @return dvojrozmerne pole
     */
    public String[][] dajZoradeneTimy() {
        int pocetTimov = this.dajPocetTimov();
        String[][] timy = new String[pocetTimov][7];

        for (int i = 0; i < pocetTimov; i++) {
            timy[i] = this.dajRiadokTimu(i);
        }

        for (int i = 0; i < timy.length - 1; i++) {
            for (int j = 0; j < timy.length - i - 1; j++) {
                if (Integer.parseInt(timy[j][7]) < Integer.parseInt(timy[j + 1][7])) {
                    String[] tmp = timy[j];
                    timy[j] = timy[j + 1];
                    timy[j + 1] = tmp;
                }
            }
        }
        
        return timy;
    }

    /**
     * Nastavi aktualne kolo
     * @param paKolo kolo
     */
    public void nastavKolo(Kolo paKolo) {
        if (paKolo != null) {
            aKolo = paKolo;
        }
    }

    /**
     * Vrati nasledujuce kolo v lige
     * @return kolo
     */
    public Kolo dajDalsieKolo() {
        if (aKolo == null) {
            return null;
        }

        Kolo dalsieKolo = aHlavnyModel.dajLigu().dajDalsieKolo(aKolo);
        if (dalsieKolo != null) {
            aKolo = dalsieKolo;
        }
        return aKolo;
    }

    /**
     * Vrati predchadzajuce kolo v lige
     * @return kolo
     */
    public Kolo dajPredosleKolo() {
        if (aKolo == null) {
            return null;
        }

        Kolo predosleKolo = aHlavnyModel.dajLigu().dajPredosleKolo(aKolo);
        if (predosleKolo != null) {
            aKolo = predosleKolo;
        }
        return aKolo;
    }

    /**
     * Vrati true ak exsituje od aktualneho kola dalsie kolo
     * @return true/false
     */
    public boolean maDalsieKolo() {
        if (aKolo == null) {
            return false;
        }
        return aHlavnyModel.dajLigu().dajDalsieKolo(aKolo) != null;
    }

    /**
     * Vrati true ak exsituje od aktualneho kola predchadzajuce kolo
     * @return true/false
     */
    public boolean maPredosleKolo() {
        if (aKolo == null) {
            return false;
        }
        return aHlavnyModel.dajLigu().dajPredosleKolo(aKolo) != null;
    }

    @Override
    public void propertyChange(PropertyChangeEvent evt) {
        this.resetujKolo();
    }
}
