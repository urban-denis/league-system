package modely;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.Iterator;
import java.util.Scanner;
import logika.*;

/**
 * Trieda pre hl. model - hl. logicky celok architektury MVC
 * Sluzi na pracu s datami - odosielanie dat kontrolerom a spracovanie dat prijatich kontrolerami
 * @author Denis
 */
public final class HlavnyModel extends Model {

    private final Divizia aDivizia;
    private Liga aLiga;

    /**
     * Konstruktor pre hl. model
     * Po vytvoreni sa nacitaju udaje zo suboroch a nasledne sa vytvaraju objekty
     */
    public HlavnyModel() {
        aDivizia = new Divizia();
        aLiga = null;
        this.nacitajUdaje();
    }

    private void nacitajUdaje() {
        File subor = new File("data/ligy.txt");
        File subor2 = new File("data/timy.txt");
        File subor3 = new File("data/zapasy.txt");

        Scanner citac1 = null;
        Scanner citac2 = null;
        Scanner citac3 = null;

        try {
            citac1 = new Scanner(subor);
            citac1.useDelimiter(",");

            citac2 = new Scanner(subor2);
            citac2.useDelimiter(",");

            citac3 = new Scanner(subor3);
            citac3.useDelimiter(",");

            while (citac1.hasNextLine()) {
                Liga liga = new Liga(citac1.nextLine());
                aDivizia.pridajLigu(liga);
            }

            while (citac2.hasNextLine()) {
                Tim tim = new Tim(citac2.next());
                Liga liga = aDivizia.dajLigu(Integer.parseInt(citac2.next()));
                liga.vlozTim(tim);
                citac2.nextLine();
            }

            while (citac3.hasNextLine()) {
                int cisloLigy = Integer.parseInt(citac3.next());
                int cisloKola = Integer.parseInt(citac3.next());
                int cisloTim1 = Integer.parseInt(citac3.next());
                int cisloTim2 = Integer.parseInt(citac3.next());
                int skoreDomacich = Integer.parseInt(citac3.next());
                int skoreHosti = Integer.parseInt(citac3.next());

                Liga liga = aDivizia.dajLigu(cisloLigy);
                Kolo kolo = null;

                if (!liga.jeKolo(cisloKola)) {
                    kolo = new Kolo(cisloKola);
                    liga.vlozKolo(kolo);
                } else if (liga.dajKolo(cisloKola) != null) {
                    kolo = liga.dajKolo(cisloKola);
                }

                Tim timDomacich = liga.dajTim(cisloTim1);
                Tim timHosti = liga.dajTim(cisloTim2);
                
                if(timDomacich != null && timHosti != null) {
                    Zapas zapas = new Zapas(timDomacich, timHosti, skoreDomacich, skoreHosti);
                    kolo.vlozZapas(zapas);
                }
                citac3.nextLine();
            }

            if (aDivizia.dajLigu(0) != null) {
                aLiga = aDivizia.dajLigu(0);
            }

        } catch (FileNotFoundException ex) {
            System.out.println("Problém s otvorením súboru " + ex.getMessage());
        } catch (NumberFormatException ex) {
            System.out.println("Problém s formátom súboru " + ex.getMessage());
        } finally {
            if (citac1 != null) {
                citac1.close();
            }
            if (citac2 != null) {
                citac2.close();
            }
            if (citac3 != null) {
                citac3.close();
            }
        }
    }

    /**
     * Metoda vracia doposial najblizsie/maximalne odohrate kolo v lige
     * @return kolo
     */
    public Kolo dajMaxKolo() {
        if (aLiga != null) {
            return aLiga.dajMaxKolo();
        }
        return null;
    }

    /**
     * Metoda vrati aktualnu ligu s ktorou sa pracuje vo vsetkych paneloch
     * @return liga
     */
    public Liga dajLigu() {
        return aLiga;
    }

    /**
     * Metoda pre zmenu ligy s ktorou sa globalne pracuje
     * @param paLiga liga
     */
    public void zmenLigu(Liga paLiga) {
        if (paLiga != null) {
            Liga stara = aLiga;
            aLiga = paLiga;
            this.firePropertyChange(aLiga.toString(), stara, aLiga);
        }
    }

    /**
     * Metoda vracia celkovy pocet lig v systeme
     * @return pocet lig
     */
    public int dajPocetLig() {
        return aDivizia.dajPocetLig();
    }

    /**
     * Metoda vracia ligu podla indexu
     * @param paCisloLigy - index ligy v systeme
     * @return liga
     */
    public Liga dajLigu(int paCisloLigy) {
        return aDivizia.dajLigu(paCisloLigy);
    }

    /**
     * Metoda vracia tim podla indexu
     * @param paIndexTimu - index timu v lige
     * @return tim
     */
    public Tim dajTim(int paIndexTimu) {
        return aLiga.dajTim(paIndexTimu);
    }

    /**
     * Metoda vracia pocet timov v lige
     * @return pocet timov
     */
    public int dajPocetTimov() {
        return aLiga.dajPocetTimov();
    }

    /**
     * Metoda pre vlozenie timu do aktualnej ligy
     * @param paNazovTimu - nazov timu
     */
    public void vlozTim(String paNazovTimu) {
        Tim tim = new Tim(paNazovTimu);
        aLiga.vlozTim(tim);
    }

    /**
     * Metoda pre vlozenie ligy do systemu
     * @param paNazovLigy nazov ligy
     */
    public void vlozLigu(String paNazovLigy) {
        Liga liga = new Liga(paNazovLigy);
        aDivizia.pridajLigu(liga);
    }

    /**
     * Metoda pre upravu timu v lige
     * @param paTim tim, kt. sa upravuje
     * @param paNazovTimu novy nazov timu
     */
    public void upravTim(Tim paTim, String paNazovTimu) {
        paTim.zmenitNazov(paNazovTimu);
    }

    /**
     * Metoda pre upravu ligy v systeme
     * @param paLiga liga, kt. sa upravuje
     * @param paNazovLigy novy nazov ligy
     */
    public void upravLigu(Liga paLiga, String paNazovLigy) {
        paLiga.zmenitNazov(paNazovLigy);
    }

    /**
     * Metoda pre odstranenie timu z ligy
     * @param paTim tim
     */
    public void vymazTim(Tim paTim) {
        aLiga.vymazTim(paTim);
    }

    /**
     * Metoda pre odstranenie ligy zo systemu
     * @param paLiga
     */
    public void vymazLigu(Liga paLiga) {
        aDivizia.vymazLigu(paLiga);
    }

    /**
     * Metoda vracia iterator timy v aktualnej lige
     * @return iterator tim
     */
    public Iterator<Tim> dajTimy() {
        return aLiga.dajTimyIterator();
    }

    /**
     * Metoda vracia iterator kol v aktualnej lige
     * @return iterator kola
     */
    public Iterator<Kolo> dajKola() {
        return aLiga.dajKolaIterator();
    }

    /**
     * Metoda vracia pocet kol odohratych v aktualnej lige
     * @return pocet kol
     */
    public int dajPocetKol() {
        return aLiga.dajPocetKol();
    }

    /**
     * Metoda vracia kolo podla cisla kola v aktualnej lige
     * @param paCisloKola - cislo kola
     * @return kolo
     */
    public Kolo dajKolo(int paCisloKola) {
        return aLiga.dajKolo(paCisloKola);
    }

    /**
     * Metoda vracia cislo kola podla zadaneho zapasu
     * @param paZapas zapas
     * @return cislo kola
     */
    public int dajCisloKolaZapasu(Zapas paZapas) {
        return aLiga.dajKoloZapasu(paZapas).dajCislo();
    }

    /**
     * Metoda pre vlozenie zapasu do ligy
     * @param paCisloKola cislo kola zapasu
     * @param paZapas zapas
     */
    public void vlozZapas(int paCisloKola, Zapas paZapas) {
        aLiga.vlozZapas(paCisloKola, paZapas);
        this.zaznamenajZmenu();
    }

    /**
     * Metoda pre upravu zapasu v akt. lige
     * @param paCisloKola cislo kola
     * @param paZapas zapas
     * @param paTimDomaci domaci tim
     * @param paTimHostia hostujuci tim
     * @param paSkoreDomaci skore domacich
     * @param paSkoreHostia skore hosti
     */
    public void upravZapas(int paCisloKola, Zapas paZapas, Tim paTimDomaci, Tim paTimHostia, int paSkoreDomaci, int paSkoreHostia) {
        if (aLiga.dajKoloZapasu(paZapas).dajCislo() == paCisloKola) {
            paZapas.zmenZapas(paTimDomaci, paTimHostia, paSkoreDomaci, paSkoreHostia);
        } else {
            aLiga.nahradZapas(paCisloKola, paZapas);
        }
        this.zaznamenajZmenu();
    }

    /**
     * Metoda pre odstranenie zapasu z ligy
     * @param paKolo kolo v kt. bol zapas odohrany
     * @param paZapas zapas
     */
    public void vymazZapas(Kolo paKolo, Zapas paZapas) {
        paKolo.vymazZapas(paZapas);
    }

    /**
     * Metoda pre zapis dat do suborov
     */
    public void ulozAktivitu() {
        aDivizia.zapisUdaje();
    }
    
    private void zaznamenajZmenu() {
        this.firePropertyChange(aLiga.toString(), null, aLiga);
    }
}
